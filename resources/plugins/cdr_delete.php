<?php
/*
        Contributor(s):
        Luis Daniel Lucio Quiroz <dlucio@okay.com.mx>
*/

if (!class_exists('xml_import_plugin_template')) {
	require_once '../classes/xml_import_plugin_template.php';
}

if (!class_exists('cdr_delete')) {
	class cdr_delete extends xml_import_plugin_template {

		function __construct(){
			parent::__construct();
		}

		function __destruct(){
		}

		public function post($payload = ''){
//			echo 'cdr_delete::post($payload = '.print_r($payload, true).')'.PHP_EOL;
//			echo 'cdr_delete::post($payload = $payload)'.PHP_EOL;
			if (is_array($payload))
				foreach ($payload as $row){
					if ($_SESSION['cdr']['format']['text'] == "xml"){
						$xml = simplexml_load_string($row['xml']);
//						echo 'XML format'.PHP_EOL;
					}
					elseif($_SESSION['cdr']['format']['text'] == "json"){
						$j = stripslashes($row['json']);
//						echo $j.PHP_EOL;
						$xml = json_decode($j, false);
//						echo 'JSON format'.PHP_EOL;
						unset($j);
					}
					else{
//						echo 'NOT FORMAT'.PHP_EOL;
						return;
					}

					// var_dump($xml);
					if (isset($xml->variables->cdr_delete)){
						$uuid = check_str(urldecode($xml->variables->uuid));
						$sql = "DELETE FROM v_xml_cdr WHERE xml_cdr_uuid = '$uuid'";
						echo $sql;
						$db2 = new database;
						if (numeric_version() < 40500){
							if (method_exists($db2, 'prepare')){
								$prep2 = $db2->prepare($sql);
								$db2->result = $prep2->exec();
							}
							else{
								$db2->sql = $sql;
								$db2->execute();
							}
						}
						else{
							$db2->result = $db2->execute($sql, array());
						}
					}
				}
		}

		public function fields(&$importer){}

		public function xml_array($row, $leg, $xml_string){}

		public function read_files($payload = array()){
			// print 'cdr_delete::read_files($payload)'.PHP_EOL;
			try {
				$xml = simplexml_load_string($payload);
			}
			catch(Exception $e) {
				echo $e->getMessage();
			}

        }
	}
}
