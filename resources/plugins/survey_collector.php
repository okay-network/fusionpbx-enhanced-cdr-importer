<?php
/*
        Contributor(s):
        Luis Daniel Lucio Quiroz <dlucio@okay.com.mx>
*/

if (!class_exists('xml_import_plugin_template')) {
	require_once '../classes/xml_import_plugin_template.php';
}

if (file_exists($_SERVER["PROJECT_ROOT"]."/app/surveys/resources/functions/version.php")){
	require_once 'app/surveys/resources/functions/version.php';
}

if (!class_exists('survey_collector')) {
	class survey_collector extends xml_import_plugin_template {

		function __construct(){
			parent::__construct();
		}

		function __destruct(){
		}

		public function post($payload = ''){
//			echo 'survey_collector::post($payload = '.print_r($payload, true).')'.PHP_EOL;
//			echo 'survey_collector::post($payload = $payload)'.PHP_EOL;
			if (is_array($payload))
				foreach ($payload as $row){
					if ($_SESSION['cdr']['format']['text'] == "xml"){
						$xml = simplexml_load_string($row['xml']);
						echo 'XML format'.PHP_EOL;
					}
					elseif($_SESSION['cdr']['format']['text'] == "json"){
						$j = stripslashes($row['json']);
//						echo $j.PHP_EOL;
						$xml = json_decode($j, false);
						echo 'JSON format'.PHP_EOL;
						unset($j);
					}
					else{
						echo 'NOT FORMAT'.PHP_EOL;
						return;
					}

//					var_dump($xml);
					if (isset($xml->variables->survey_collector)){
						$history = array();

//						echo print_r($xml->variables->transfer_history,true).PHP_EOL;
						if (isset($xml->variables->transfer_history)){
							$h = $xml->variables->transfer_history;
							foreach ($h as $hh){
								$decoded = trim(urldecode($hh));
								$tokens = preg_split ("/:/", $decoded);
								$history[$tokens[0]] = $tokens[3];
							}
						}

						unset($h, $decoded, $tokens);

						echo '$xml->variables->dtmf_history: '.print_r($xml->variables->dtmf_history,true).PHP_EOL;
						if (isset($xml->variables->dtmf_history)){
							echo '$xml->variables->dtmf_history is set'.PHP_EOL;
							if (is_array($xml->variables->dtmf_history)){
								foreach ($xml->variables->dtmf_history as $hh){
									$decoded = trim(urldecode($hh));
									echo '$decoded: '.$decoded.PHP_EOL;
									$tokens = preg_split ("/:/", $decoded);
									$history[$tokens[0]] = $tokens[1];
								}
							}
							else{
								$decoded = trim(urldecode($xml->variables->dtmf_history));
								echo '$decoded: '.$decoded.PHP_EOL;
								$tokens = preg_split ("/:/", $decoded);
								$history[$tokens[0]] = $tokens[1];
							}
						}


						$history[$xml->variables->start_epoch] = urldecode($xml->variables->sip_to_user) . '/' . urldecode($xml->variables->domain_name) . '/XML';
						ksort($history, SORT_NUMERIC);
						echo print_r($history, true);

						$history2 = array();
						$db2 = new database;
						$survey_uuid = null;
						$survey_question_uuids = array();
						$domain_uuid = urldecode($xml->variables->domain_uuid);
						$domain_name = urldecode($xml->variables->domain_name);

						if ((strlen($domain_uuid) == 0) && (strlen($domain_name) > 0)){
							$sql = "SELECT domain_uuid FROM v_domains WHERE domain_name = '$domain_name'";
							echo $sql.PHP_EOL;
							if (numeric_version() < 40500){
								if (method_exists($db2, 'prepare')){
									$prep2 = $db2->prepare($sql);
									$db2->result = $prep2->execute();
								}
								else{
									$db2->select($sql);
								}
                              					}
							else{
								$db2->result = $db2->select($sql, array(), 'all');
							}
							if (is_uuid($db2->result[0]['domain_uuid'])){
								$domain_uuid = $db2->result[0]['domain_uuid'];
							}
							else{
								echo 'We cant find domain_uuid in the DB';
							}
						}
						echo "domain_uuid: $domain_uuid".PHP_EOL;

						foreach ($history as $t => $e){
							print $t . ':' . $e . PHP_EOL;
							if (preg_match('/([\w\d\.\-_]+)\/([\w\d\.\-_]+)\/XML/', $e, $matches)){
								//echo print_r($matches, true).PHP_EOL;
								$extension = $matches[1];
								$context = $matches[2];
								$history2[$extension] = null;

								if (is_null($survey_uuid)){
									// Find the first the survey UUID
									$sql = "SELECT survey_uuid FROM v_ivr_menus i INNER JOIN v_domains d USING (domain_uuid) INNER JOIN v_surveys s USING (ivr_menu_uuid) WHERE i.ivr_menu_extension='$extension' AND d.domain_name='$context' AND s.survey_enabled='true'";
									echo $sql.PHP_EOL;
									if (numeric_version() < 40500){
										if (method_exists($db2, 'prepare')){
											$prep2 = $db2->prepare($sql);
											$db2->result = $prep2->execute();
										}
										else{
											$db2->select($sql);
										}
                                					}
									else{
										$db2->result = $db2->select($sql, array(), 'all');
									}

									if (is_uuid($db2->result[0]['survey_uuid'])){
										$survey_uuid = $db2->result[0]['survey_uuid'];
									}
								}

								// Find the first the survey UUID
								$sql = "SELECT ivr_menu_uuid FROM v_ivr_menus i INNER JOIN v_domains d USING (domain_uuid) WHERE i.ivr_menu_extension='$extension' AND d.domain_name='$context'";
								echo $sql.PHP_EOL;
								if (numeric_version() < 40500){
									if (method_exists($db2, 'prepare')){
										$prep2 = $db2->prepare($sql);
										$db2->result = $prep2->execute();
									}
									else{
										$db2->select($sql);
									}
                               					}
								else{
									$db2->result = $db2->select($sql, array(), 'all');
								}

								if (is_uuid($db2->result[0]['ivr_menu_uuid'])){
									$survey_question_uuids[$extension] = $db2->result[0]['ivr_menu_uuid'];
								}

							}
							elseif (preg_match('/([\d\*#]+)/', $e, $matches)){
								//echo print_r($matches, true).PHP_EOL;
								$history2[$extension] .= $matches[1];
							}
							unset($matches);
						}

						echo "survey_uuid: $survey_uuid".PHP_EOL;
						echo print_r($history2,true).PHP_EOL;
						echo print_r($survey_question_uuids,true).PHP_EOL;

						$xml_cdr_uuid = $row['xml_cdr_uuid'];
						if (strlen($xml_cdr_uuid) == 0){
							$xml_cdr_uuid = check_str(urldecode($xml->variables->uuid));
						}
						echo "xml_cdr_uuid: $xml_cdr_uuid".PHP_EOL;

						foreach ($history2 as $question => $answer){
							if (strlen($answer) > 0){
								$survey_question_answer_uuid = uuid();
								$answer_as_number = floatval(preg_replace('/\*/','.',$answer, 1));
								$sql = "INSERT INTO v_survey_question_answers(survey_question_answer_uuid, survey_question_uuid, survey_uuid, domain_uuid, xml_cdr_uuid, survey_question_answer_as_text, survey_question_answer_as_number) 
VALUES ('$survey_question_answer_uuid', '".$survey_question_uuids[$question]."', '$survey_uuid', '$domain_uuid', '$xml_cdr_uuid', '$answer', $answer_as_number)";
								echo $sql.PHP_EOL;

								if (numeric_version() < 40500){
									if (method_exists($db2, 'prepare')){
										$prep2 = $db2->prepare($sql);
										$db2->result = $prep2->exec();
									}
									else{
										$db2->sql = $sql;
										$db2->execute();
									}
								}
								else{
									$db2->result = $db2->execute($sql, array());
								}
							}
						}
					}
				}
		}

		public function fields(&$importer){}

		public function xml_array($row, $leg, $xml_string){}

		public function read_files($payload = array()){
			print 'survey_collector::read_files('.print_r($payload,true).')'.PHP_EOL;
			$this->post($payload);
        	}
	}
}
