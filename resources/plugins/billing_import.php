<?php
/*
        Contributor(s):
        Luis Daniel Lucio Quiroz <dlucio@okay.com.mx>
*/

if (!class_exists('xml_import_plugin_template')) {
	require_once '../classes/xml_import_plugin_template.php';
}

if (file_exists($_SERVER["PROJECT_ROOT"]."/app/billing/app_config.php")){
	require_once 'app/billing/resources/functions/version.php';
	require_once 'app/billing/resources/functions/deals.php';
	require_once 'app/billing/resources/functions/misc.php';
}

if (!class_exists('billing_import')) {
class billing_import extends xml_import_plugin_template {

	private $items;

	function __construct(){
		parent::__construct();
		$this->items = array();
	}

	function __destruct(){
	}

	public function fields(&$importer){
		print 'billing_import::fields($importer)'.PHP_EOL;
		if (!in_array('call_buy', $importer->fields))
			$importer->fields[] = 'call_buy';

		if (!in_array('call_sell', $importer->fields))
			$importer->fields[] = 'call_sell';

		if (!in_array('carrier_name', $importer->fields))
			$importer->fields[] = 'carrier_name';

		if (!in_array('call_sell_local_currency', $importer->fields))
			$importer->fields[] = 'call_sell_local_currency';

		if (!in_array('local_currency', $importer->fields))
			$importer->fields[] = 'local_currency';
		$importer->fields[] = 'billing_status';
		$importer->fields[] = 'billing_json';
	}

	public function xml_array($row, $leg, $xml_string){
	}

	public function read_files($payload = array()){
		// print 'billing_import::read_files($payload)'.PHP_EOL;
		try {
			$xml = simplexml_load_string($payload);
		}
		catch(Exception $e) {
			echo $e->getMessage();
		}

		$this->execute($xml);
	}

	public function pre_post(){
		// print 'billing_import::pre_post()'.PHP_EOL;
		$xml_string = trim($_POST["cdr"]);
		$xml_lines = explode("\n", $xml_string);
		libxml_use_internal_errors(true);
		try {
			$xml = simplexml_load_string($xml_string);
		}
		catch(Exception $e) {
			echo $e->getMessage();
		}

		if ($xml === FALSE){
			$errors = libxml_get_errors();
			foreach ($errors as $error) {
				echo $this->display_xml_error($error, $xml_lines);
			}
			libxml_clear_errors();
		}

		return $this->execute($xml);
	}

	public function post($payload = ''){
	}

	public function execute(&$xml, $post_status = null){
		$debug = true; $billing_status = BILLING_UNBILLED;
		print 'billing_import::execute($xml, $post_status = '.$post_status.')'.PHP_EOL;
	//	var_dump($xml->variables);
	//	foreach ($xml as $cdr){
			//print_r($cdr);

			$execute = 0;
			// TODO: Look for rawurldecode
			// $j = stripslashes($xml->variables->json);
			// $local_json = json_decode($j, true);

			if (!isset($xml->variables->cdr_billing)){
				print "cdr_billing does not exist, skipping\n";
				return;
			}

			if (file_exists($_SERVER["PROJECT_ROOT"]."/app/billing/app_config.php")){
				require_once 'app/billing/resources/functions/rating.php';
				$execute = 1;
			}
			else{
				print "billing not detected, skipping\n";
			}

			if ($execute){
				$billing_status |= BILLING_BILLED;
				$db2 = new database;
				$db2->table = "v_billings";
				if (isset($xml->variables->lcr_currency)){
					$lcr_currency = urldecode($xml->variables->lcr_currency);
				}
				else{
					$lcr_currency = (strlen($_SESSION['billing']['currency']['text'])?$_SESSION['billing']['currency']['text']:'USD');
				}
				$default_currency = (strlen($_SESSION['billing']['currency']['text'])?$_SESSION['billing']['currency']['text']:'USD');
				$accountcode = (strlen(urldecode($xml->variables->accountcode)))?check_str(urldecode($xml->variables->accountcode)):$domain_name;
				$call_direction = check_str(urldecode($xml->variables->call_direction));

				// Get the bililng profile
				$sql_billing_profile = "SELECT * FROM v_billings WHERE type_value='$accountcode'";
				if ($debug){
					echo '<hr />'.PHP_EOL;
					echo $sql_billing_profile; echo '<br />'.PHP_EOL;
				}
				if (numeric_version() < 40500){
					if (method_exists($db2, 'prepare')){
						$prep2 = $db2->prepare($sql_billing_profile);
						$db2_result = $prep2->execute();
					}
					else{
						$db2->select($sql_billing_profile);
					}
				}
				else{
					$db2_result = $db2->select($sql_billing_profile, array(), 'all');
				}

				$billing_profile =  $db2_result[0];
				$billing_array['profile'] = $billing_profile;

				if ($debug){
					echo print_r($billing_profile, true).PHP_EOL;
				}

				switch($call_direction){
					case 'outbound':
							if (!isset($xml->variables->lcr_query_digits)){
								$destination_number = check_str(urldecode($xml->variables->caller_destination));
								if (($destination_number[0] == 0) && isset($_SESSION['billing']['default_country_code']['numeric'])){
									$destination_number = $_SESSION['billing']['default_country_code']['numeric'] . substr($destination_number, 1);
								}
								if ($debug) {
									echo "destination_number from caller_destination ".$destination_number.PHP_EOL;
								}

							}
							else{
								$destination_number = check_str(urldecode($xml->variables->lcr_query_digits));
								if ($debug) {
									echo "destination_number from lcr_query_digits ".$destination_number.PHP_EOL;
								}
							}
							$target_number = $destination_number;
							$destination_number_serie = number_series($destination_number);
							$carrier_name = check_str(urldecode($xml->variables->lcr_carrier));
							if (!isset($xml->variables->lcr_rate)){
								$lcr_rate = 0;
							}
							else{
								$lcr_rate = check_str(urldecode($xml->variables->lcr_rate));
							}
							$sql_rate ="SELECT v_lcr.connect_increment, v_lcr.talk_increment, v_lcr.currency FROM v_lcr, v_carriers WHERE v_carriers.carrier_name = '".$xml->variables->lcr_carrier."' AND v_lcr.rate=".$lcr_rate." AND v_lcr.lcr_direction = '".check_str(urldecode($xml->variables->call_direction))."' AND digits IN ($destination_number_serie) AND v_lcr.carrier_uuid = v_carriers.carrier_uuid  ORDER BY digits DESC, rate ASC limit 1";
							$sql_user_rate = "SELECT v_lcr.connect_rate, v_lcr.rate, v_lcr.currency, connect_increment, talk_increment, v_billings.billing_uuid, v_billings.balance as debt, v_billings.domain_uuid FROM v_lcr JOIN v_billings ON v_billings.type_value='$accountcode' WHERE v_lcr.carrier_uuid IS NULL AND v_lcr.lcr_direction = '".check_str(urldecode($xml->variables->call_direction))."' AND v_lcr.lcr_profile=v_billings.lcr_profile AND NOW() >= v_lcr.date_start AND NOW() < v_lcr.date_end AND digits IN ($destination_number_serie) ORDER BY digits DESC, date_start DESC, date_end ASC, rate DESC LIMIT 1";
							if ($debug) {
								echo "sql_rate: $sql_rate\n";
								echo "sql_user_rate: $sql_user_rate\n";
							}

							if (numeric_version() < 40500){
								if (method_exists($db2, 'prepare')){
									$prep2 = $db2->prepare($sql_rate);
									$db2_result = $prep2->execute();
								}
								else{
									$db2->select($sql_rate);
								}
							}
							else{
								$db2_result = $db2->select($sql_rate, array(), 'all');
							}
							//print_r($db2_result);
							if (isset($xml->variables->lcr_currency)){
								$lcr_currency = urldecode($xml->variables->lcr_currency);
							}
							else{
								$lcr_currency = (strlen($db2_result[0]['currency'])?check_str($db2_result[0]['currency']):
									(strlen($_SESSION['billing']['currency']['text'])?$_SESSION['billing']['currency']['text']:'USD')
								);
							}
							$lcr_rate = (strlen($xml->variables->lcr_rate)?$xml->variables->lcr_rate:0);
							$lcr_second_rate = (strlen($xml->variables->lcr_second_rate)?$xml->variables->lcr_second_rate:null);
							$lcr_first_increment = (strlen($db2_result[0]['connect_increment'])?check_str($db2_result[0]['connect_increment']):60);
							$lcr_second_increment = (strlen($db2_result[0]['talk_increment'])?check_str($db2_result[0]['talk_increment']):60);

							if (numeric_version() < 40500){
								if (method_exists($db2, 'prepare')){
									$prep2 = $db2->prepare($sql_user_rate);
									$db2_result = $prep2->execute();
								}
								else{
									$db2->select($sql_user_rate);
								}
							}
							else{
								$db2_result = $db2->select($sql_user_rate, array(), 'all');
							}
							$billing_uuid = (strlen($db2_result[0]['billing_uuid']))?($db2_result[0]['billing_uuid']):'';
							$domain_uuid = (strlen($db2_result[0]['domain_uuid']))?($db2_result[0]['domain_uuid']):'';
							$debt = (strlen($db2_result[0]['debt']))?($db2_result[0]['debt']):'';
							$lcr_user_rate = (strlen($xml->variables->lcr_user_first_rate)?$xml->variables->lcr_user_first_rate:(strlen($xml->variables->lcr_user_rate)?$xml->variables->lcr_user_rate:((strlen($db2_result[0]['connect_rate']))?($db2_result[0]['connect_rate']):0.01)));
							$lcr_user_second_rate = (strlen($xml->variables->lcr_user_second_rate)?$xml->variables->lcr_user_second_rate:((strlen($db2_result[0]['rate']))?($db2_result[0]['rate']):null));
							$lcr_user_first_increment = (isset($xml->variables->lcr_user_first_increment)?$xml->variables->lcr_user_first_increment:(strlen($db2_result[0]['connect_increment'])?check_str($db2_result[0]['connect_increment']):60));
							$lcr_user_second_increment = (isset($xml->variables->lcr_user_second_increment)?$xml->variables->lcr_user_second_increment:(strlen($db2_result[0]['talk_increment'])?check_str($db2_result[0]['talk_increment']):60));
							$lcr_user_currency = (strlen($xml->variables->lcr_user_currency)?$xml->variables->lcr_user_currency:(strlen($db2_result[0]['currency'])?check_str($db2_result[0]['currency']):(strlen($_SESSION['billing']['currency']['text'])?$_SESSION['billing']['currency']['text']:'USD')));
							$lcr_user_original_currency = $db2_result[0]['currency'];

							if (isset($xml->variables->lcr_user_concept)){
								$description = urldecode($xml->variables->lcr_user_concept);
							}
							else{
								$_add = '';
								if (isset($_SESSION['billing']['whmcs_show_outgoing_caller_in_invoice']['boolean']) && ($_SESSION['billing']['whmcs_show_outgoing_caller_in_invoice']['boolean'] == 'true')){
									$sip_from_user = check_str(urldecode($xml->variables->sip_from_user));
									$outbound_caller_id_number = check_str(urldecode($xml->variables->outbound_caller_id_number));
									$_add = ' from '.$sip_from_user.'('.$outbound_caller_id_number.')';
								}
								$description = 'Outbound call'.$_add.' to '.$destination_number;
							}
							break;
					case 'inbound':
							$callee_number = check_str(urldecode($xml->variables->caller_destination));
							$callee_number = preg_replace('/\D/i', '', $callee_number);
							$target_number = $callee_number;
							$callee_number_serie = number_series($callee_number);
							$sql_user_rate = "SELECT v_lcr.currency, v_lcr.rate, v_lcr.connect_increment, v_lcr.talk_increment, v_lcr.connect_rate, v_billings.billing_uuid, v_billings.balance as debt, v_billings.domain_uuid FROM v_lcr JOIN v_billings ON v_billings.type_value='$accountcode' WHERE v_lcr.carrier_uuid IS NULL AND v_lcr.lcr_direction = '".check_str(urldecode($xml->variables->call_direction))."' AND v_lcr.lcr_profile=v_billings.lcr_profile AND NOW() >= v_lcr.date_start AND NOW() < v_lcr.date_end AND digits IN ($callee_number_serie) ORDER BY digits DESC, rate ASC, date_start DESC LIMIT 1";

							if ($debug) {
								echo "sql_user_rate: $sql_user_rate\n";
							}

							if (numeric_version() < 40500){
								if (method_exists($db2, 'prepare')){
									$prep2 = $db2->prepare($sql_user_rate);
									$db2_result = $prep2->execute();
								}
								else{
									$db2->select($sql_user_rate);
								}
							}
							else{
								$db2_result = $db2->select($sql_user_rate, array(), 'all');
							}

							// If selling rate is found, then we fill with data, otherwise rate will be 0
							if (isset($xml->variables->lcr_currency)){
								$lcr_currency = urldecode($xml->variables->lcr_currency);
							}
							else{
								$lcr_currency = (strlen($db2_result[0]['currency'])?check_str($db2_result[0]['currency']):
									(strlen($_SESSION['billing']['currency']['text'])?$_SESSION['billing']['currency']['text']:'USD')
								);
							}
							$billing_uuid = (strlen($db2_result[0]['billing_uuid']))?($db2_result[0]['billing_uuid']):'';
							$domain_uuid = (strlen($db2_result[0]['domain_uuid']))?($db2_result[0]['domain_uuid']):'';
							$debt = (strlen($db2_result[0]['debt']))?($db2_result[0]['debt']):'';
							$lcr_user_rate = (strlen($xml->variables->lcr_user_first_rate)?$xml->variables->lcr_user_first_rate:(strlen($xml->variables->lcr_user_first_rate)?$xml->variables->lcr_user_rate:((strlen($db2_result[0]['connect_rate']))?($db2_result[0]['connect_rate']):0)));
							$lcr_user_second_rate = (strlen($xml->variables->lcr_user_second_rate)?$xml->variables->lcr_user_second_rate:((strlen($db2_result[0]['rate']))?($db2_result[0]['rate']):null));
							$lcr_user_first_increment = (strlen($xml->variables->lcr_user_first_increment)?$xml->variables->lcr_user_first_increment:(strlen($db2_result[0]['connect_increment'])?check_str($db2_result[0]['connect_increment']):60));
							$lcr_user_second_increment = (strlen($xml->variables->lcr_user_second_increment)?$xml->variables->lcr_user_second_increment:(strlen($db2_result[0]['talk_increment'])?check_str($db2_result[0]['talk_increment']):60));
							$lcr_user_currency = (strlen($xml->variables->lcr_user_currency)?$xml->variables->lcr_user_currency:(strlen($db2_result[0]['currency'])?check_str($db2_result[0]['currency']):(strlen($_SESSION['billing']['currency']['text'])?$_SESSION['billing']['currency']['text']:'USD')));
							$lcr_user_original_currency = $db2_result[0]['currency'];

							// Actually, there is no way to detect what carrier is the calling comming from using current information
							$lcr_rate = 0; $lcr_second_rate = 0; $lcr_first_increment = 0; $lcr_second_increment = 0;
							if (isset($xml->variables->lcr_user_concept)){
								$description = urldecode($xml->variables->lcr_user_concept);
							}
							else{
								$_add = '';
								if (isset($_SESSION['billing']['whmcs_show_incoming_callee_in_invoice']['boolean']) && ($_SESSION['billing']['whmcs_show_incoming_callee_in_invoice']['boolean'] == 'true')){
									$sip_from_user = check_str(urldecode($xml->variables->sip_from_user));
									$_add = ' to '.$sip_from_user;
								}
								$description = 'Inbound call to '.$callee_number.$_add;;
							}
							break;
					case 'local':
							if (!isset($xml->variables->lcr_query_digits)){
								$destination_number = check_str(urldecode($xml->variables->caller_destination));
							}
							else{
								$destination_number = check_str(urldecode($xml->variables->lcr_query_digits));
							}
							$target_number = $destination_number;
							$destination_number_serie = number_series($destination_number);
							$sql_user_rate = "SELECT v_lcr.currency, connect_increment, talk_increment, connect_rate, rate, v_billings.billing_uuid, v_billings.balance as debt, v_billings.domain_uuid FROM v_lcr JOIN v_billings ON v_billings.type_value='$accountcode' WHERE v_lcr.carrier_uuid IS NULL AND v_lcr.lcr_direction = '".check_str(urldecode($xml->variables->call_direction))."' AND v_lcr.lcr_profile=v_billings.lcr_profile AND NOW() >= v_lcr.date_start AND NOW() < v_lcr.date_end AND digits IN ($destination_number_serie) ORDER BY digits DESC, rate ASC, date_start DESC LIMIT 1";
							if ($debug) {
								echo "sql_user_rate: $sql_user_rate\n";
							}

							if (numeric_version() < 40500){
								if (method_exists($db2, 'prepare')){
									$prep2 = $db2->prepare($sql_user_rate);
									$db2_result = $prep2->execute();
								}
								else{
									$db2->select($sql_user_rate);
								}
							}
							else{
								$db2_result = $db2->select($sql_user_rate, array(), 'all');
							}

							// If selling rate is found, then we fill with data, otherwise rate will be 0
							if (isset($xml->variables->lcr_currency)){
								$lcr_currency = urldecode($xml->variables->lcr_currency);
							}
							else{
								$lcr_currency = (strlen($db2_result[0]['currency'])?check_str($db2_result[0]['currency']):
									(strlen($_SESSION['billing']['currency']['text'])?$_SESSION['billing']['currency']['text']:'USD')
								);
							}
							$billing_uuid = (strlen($db2_result[0]['billing_uuid']))?($db2_result[0]['billing_uuid']):'';
							$domain_uuid = (strlen($db2_result[0]['domain_uuid']))?($db2_result[0]['domain_uuid']):'';
							$debt = (strlen($db2_result[0]['debt']))?($db2_result[0]['debt']):'';
							$lcr_user_rate = (strlen($xml->variables->lcr_user_first_rate)?$xml->variables->lcr_user_first_rate:(strlen($xml->variables->lcr_user_rate)?$xml->variables->lcr_user_rate:((strlen($db2_result[0]['connect_rate']))?($db2_result[0]['connect_rate']):0)));
							$lcr_user_second_rate = (strlen($xml->variables->lcr_user_second_rate)?$xml->variables->lcr_user_second_rate:((strlen($db2_result[0]['rate']))?($db2_result[0]['rate']):null));
							$lcr_user_first_increment = (strlen($xml->variables->lcr_user_first_increment)?$xml->variables->lcr_user_first_increment:(strlen($db2_result[0]['connect_increment'])?check_str($db2_result[0]['connect_increment']):60));
							$lcr_user_second_increment = (strlen($xml->variables->lcr_user_second_increment)?$xml->variables->lcr_user_second_increment:(strlen($db2_result[0]['talk_increment'])?check_str($db2_result[0]['talk_increment']):60));
							$lcr_user_currency = (strlen($xml->variables->lcr_user_currency)?$xml->variables->lcr_user_currency:(strlen($db2_result[0]['currency'])?check_str($db2_result[0]['currency']):(strlen($_SESSION['billing']['currency']['text'])?$_SESSION['billing']['currency']['text']:'USD')));
							$lcr_user_original_currency = $db2_result[0]['currency'];

							// Actually, internal calls have 0 cost
							$lcr_rate = 0; $lcr_second_rate = 0; $lcr_first_increment = 0; $lcr_second_increment = 0;
							if (isset($xml->variables->lcr_user_concept)){
								$description = urldecode($xml->variables->lcr_user_concept);
							}
							else{
								$_add = '';
								if (isset($_SESSION['billing']['whmcs_show_local_caller_in_invoice']['boolean']) && ($_SESSION['billing']['whmcs_show_local_alcaller_in_invoice']['boolean'] == 'true')){
									$sip_from_user = check_str(urldecode($xml->variables->sip_from_user));
									$_add = ' from '.$sip_from_user;
								}
								$description = 'Internal call'.$_add.' to '.$destination_number;
							}
							break;
				}
				unset($db2_result);

				// Please note that we save values using LCR currency, but we discount balance in billing currency

				$time = check_str(urldecode($xml->variables->billsec));
				$start_stamp = check_str(urldecode($xml->variables->start_stamp));

				if (isset($_SESSION['billing']['whmcs_show_accountcode_in_invoice']['boolean']) && ($_SESSION['billing']['whmcs_show_accountcode_in_invoice']['boolean'] == 'true')){
					$description .= (' #'.$accountcode);
				}

				if (isset($_SESSION['billing']['whmcs_show_timestamp_in_invoice']['boolean']) && ($_SESSION['billing']['whmcs_show_timestamp_in_invoice']['boolean'] == 'true')){
					$description .= (' @'.$start_stamp);
				}

				// if not set, it saves space in the invoice
				if (isset($_SESSION['billing']['whmcs_show_duration_in_invoice']['boolean']) && ($_SESSION['billing']['whmcs_show_duration_in_invoice']['boolean'] == 'true')){
					$description .= (' for '.$time.' secs');
				}

				if ($debug){
					echo "call_cost($lcr_rate, $lcr_first_increment, $lcr_second_increment, $time);".PHP_EOL;
					echo "call_cost($lcr_user_rate, $lcr_user_first_increment, $lcr_user_second_increment, $time);".PHP_EOL;
				}

				$call_buy = call_cost($lcr_rate, $lcr_first_increment, $lcr_second_increment, $time);
				$call_sell = call_cost($lcr_user_rate, $lcr_user_first_increment, $lcr_user_second_increment, $time);

				$billing_array['buying']['lcr_first_rate'] = floatval($lcr_rate);
				$billing_array['buying']['lcr_second_rate'] = floatval($lcr_second_rate);
				$billing_array['buying']['lcr_first_increment'] = intval($lcr_first_increment);
				$billing_array['buying']['lcr_second_increment'] = intval($lcr_second_increment);
				$billing_array['buying']['time'] = $time;
				$billing_array['buying']['cost'] = $call_buy;
				$billing_array['buying']['currency'] = $lcr_currency;

				$billing_array['selling']['lcr_first_rate'] = floatval($lcr_user_rate);
				$billing_array['selling']['lcr_second_rate'] = floatval($lcr_user_second_rate);
				$billing_array['selling']['lcr_first_increment'] = intval($lcr_user_first_increment);
				$billing_array['selling']['lcr_second_increment'] = intval($lcr_user_second_increment);
				$billing_array['selling']['time'] = $time;
				$billing_array['selling']['original_cost'] = $call_sell;
				$billing_array['selling']['original_currency'] = $lcr_user_original_currency;

				// Costs/Sell call are in original LCR currency, they need to be converted

				$call_buy  = check_str($call_buy);
				$call_sell = check_str($call_sell);

				$sql_billing_profile_currency = "SELECT billing_cycle, balance, old_balance, currency, whmcs_user_id, pay_days FROM v_billings WHERE type_value='$accountcode' LIMIT 1";
				if ($debug) {
					echo "sql_billing_profile_currency: $sql_billing_profile_currency\n";
				}
				if (numeric_version() < 40500){
					if (method_exists($db2, 'prepare')){
						$prep2 = $db2->prepare($sql_billing_profile_currency);
						$db2_result = $prep2->execute();
					}
					else{
						$db2->select($sql_billing_profile_currency);
					}
				}
				else{
					$db2_result = $db2->select($sql_billing_profile_currency, array(), 'all');
				}
				if ($debug) {
					print_r($db2_result);
				}
				$actual_currency = (strlen($lcr_currency)?
								$lcr_currency:
								(strlen($_SESSION['billing']['currency']['text'])?$_SESSION['billing']['currency']['text']:'USD')
				);
				$billing_currency = (strlen($db2_result[0]['currency'])?$db2_result[0]['currency']:$default_currency);

				$balance = $db2_result[0]['balance'];
				$old_balance = $db2_result[0]['old_balance'];
				$whmcs_user_id = $db2_result[0]['whmcs_user_id'];
				$pay_days = $db2_result[0]['pay_days'];
				$billing_cycle = $db2_result[0]['billing_cycle'];

				if ($debug) {
					echo "sql: " . $sql_billing_profile_currency . "\n";
					echo "carrier ".$carrier_name."\n";
					echo "time $time\n";
					echo "buy r:$lcr_rate - $lcr_first_increment - $lcr_first_increment = $call_buy\n";
					echo "ssell r:$lcr_user_rate - $lcr_user_first_increment - $lcr_user_second_increment = $call_sell\n";
					echo "lcr currency $lcr_currency\n";
					echo "actual currency $actual_currency\n";
					echo "user currency $lcr_user_currency\n";
					echo "billing currency $billing_currency\n";
					echo "bal: $balance\n";
					echo "old bal: $old_balance\n";
					echo "whmcs_user_id: $whmcs_user_id\n";
					echo "pay_days: $pay_days\n";
					echo "billing_cycle: $billing_cycle\n";
					echo "billing_uuid: $billing_uuid\n";
					echo "accountcode: $accountcode \n";
					echo "target_number: $target_number\n";
					echo "call_direction: $call_direction\n";
				}

				$billing_call_sell = currency_convert($call_sell, $billing_currency, $lcr_user_currency);
				$billing_array['selling']['cost'] = $billing_call_sell;
				$billing_array['selling']['currency'] = $billing_currency;

				if (
				(	(isset($_SESSION['billing']['deals_realtime']['boolean'])) && 
					($_SESSION['billing']['deals_realtime']['boolean'] == 'true') && 
					(is_null($post_status) or strlen($post_status) == 0)
				) ||
				(	(!is_null($post_status)) && 
					(($post_status & BILLING_DEAL_PROCESSED) != BILLING_DEAL_PROCESSED))
				){
					if ($debug){
						echo 'Reviewing for a deal'.PHP_EOL;
					}
					$deal = null;
					$special_sale_rate =  deal_price($billing_profile, $target_number, $call_direction, $billing_status, $deal);	// New price is already using billing profile currency
					if ($debug){
						echo 'special_sale_rate: ['.$special_sale_rate.'] strlen:'.strlen($special_sale_rate).PHP_EOL;
					}
					if (!is_null($deal)){
						$billing_call_sell = call_cost($special_sale_rate, $lcr_user_first_increment, $lcr_user_second_increment, $time);
						$billing_status |= BILLING_DEAL_PROCESSED;
						$description .= ' *DEAL '.$deal['label'];
						$billing_array['deal'] = $deal;
						if ($debug){
							echo print_r($deal, true).PHP_EOL;
						}
					}
				}
				else{
					if ($debug){
						echo 'No need to look for deals, deals_realtime is false'.PHP_EOL;
					}
				}

				if ($debug) {
					echo "bcs: $billing_call_sell $billing_currency\n";
				}

				// Lets build the $items array
				$this->items[0]['description'] = $description;
				$this->items[0]['amount'] = $billing_call_sell;
				$this->items[0]['taxed'] = 1;

					// Short duration calls
					$complement = 0;
					if (isset($_SESSION['billing']['short_duration_time']['numeric'])){
						$short_duration_time = intval($_SESSION['billing']['short_duration_time']['numeric']);
						$short_duration_charge = floatval($_SESSION['billing']['short_duration_charge']['numeric']);
						$short_duration_currency = (strlen($_SESSION['billing']['short_duration_currency']['text'])?$_SESSION['billing']['short_duration_currency']['text']:'USD');

						if ($short_duration_charge > 0 && $time > 0 && $time <= $short_duration_time){

							$sql_short = "select accountcode, sum(case when billsec = 0 then 1 else 0 end) as z, sum(case when billsec between 1 and 6 then 1 else 0 end) as s, sum(case when billsec > 6 then 1 else 0 end) as l from v_xml_cdr where accountcode='$accountcode' and start_stamp >= DATE_SUB(NOW(), INTERVAL 1 DAY)";
							if ($debug) {
								echo "sql_short: $sql_short\n";
							}

							if (numeric_version() < 40500){
								if (method_exists($db2, 'prepare')){
									$prep2 = $db2->prepare($sql_short);
									$db2_result = $prep2->execute();
								}
								else{
									$db2->select($sql_short);
								}
							}
							else{
								$db2_result = $db2->select($sql_short, array(), 'all');
							}

							$short_percentage = $db2_result[0]['s'] / ($db2_result[0]['s'] + $db2_result[0]['l']);
							$short_percentage *= 100;
							$short_duration_acceptable_percentage = isset($_SESSION['billing']['short_duration_acceptable_percentage']['numeric'])?floatval($_SESSION['billing']['short_duration_acceptable_percentage']['numeric']):0;
							if ($debug) {
								print_r($db2_result);
								echo "short_percentage: $short_percentage\n";
								echo "short_duration_acceptable_percentage: $short_duration_acceptable_percentage\n";
							}

							if ($short_percentage >= $short_duration_acceptable_percentage){
								// Charge
								$extra_payload = array();
								$extra_payload['action'] = 'AddBillableItem';
								$extra_payload['identifier'] = $username;
								$extra_payload['secret'] = $password;
								$extra_payload['clientid'] = $whmcs_user_id;
								$extra_payload['description'] = $description . ' short duration ('.$time.' seconds)';
								$extra_payload['amount'] = round($short_duration_charge * currency_convert_rate($billing_currency,$short_duration_currency), 2);
								$extra_payload['invoiceaction'] = 'nextcron';	// Default Action
								$extra_payload['responsetype'] = 'json';
								$complement = $extra_payload['amount'];

								$this->items[1]['description'] = $description . ' short duration ('.$time.' seconds)';
								$this->items[1]['amount'] = round($short_duration_charge * currency_convert_rate($billing_currency,$short_duration_currency), 2);;
								$this->items[1]['taxed'] = 1;
							}
						}
					}
			} // if(execute)
//		}

		$billing_array['items'] = $this->items;
		$billing_json = json_encode($billing_array, JSON_NUMERIC_CHECK|JSON_PRESERVE_ZERO_FRACTION|JSON_PRETTY_PRINT);
		if ($debug) {
			echo 'this->items: '. print_r($this->items, true).PHP_EOL;
		}

		if (!is_null($whmcs_user_id)){
			whmcs_charge($billing_profile, $this->items);
		}
		internal_charge($billing_profile, $this->items);

		$answer['call_buy'] = $call_buy;
		$answer['call_sell'] = $call_sell;
		$answer['carrier_name'] = $carrier_name;
		$answer['call_sell_local_currency'] = $billing_call_sell;
		$answer['local_currency'] = $billing_currency;
		$answer['billing_status'] = BILLING_BILLED;
		$answer['billing_json'] = $billing_json;
		return array($answer);
	} // foreach
}
}
