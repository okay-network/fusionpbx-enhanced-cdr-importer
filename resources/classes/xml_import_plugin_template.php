<?php /*
        Contributor(s):
        Luis Daniel Lucio Quiroz <dlucio@okay.com.mx>
*/

if (!class_exists('xml_import_plugin_template')) {
	abstract class xml_import_plugin_template {
		abstract public function fields(&$importer);
		abstract public function xml_array($row, $leg, $xml_string);
		abstract public function read_files($payload = '');
		abstract public function post($payload = '');

		protected $importer;

		function __construct(){
			openlog(get_class(), LOG_PID | LOG_PERROR, LOG_LOCAL0);
			$a = func_get_args();
			$i = func_num_args();
			if (method_exists($this,$f='__construct'.$i)) {
				call_user_func_array(array($this,$f),$a);
			}
			else{
				throw new Exception('Incorrect number of parameters, 0, 2 or 4 only.');
			}
			//reload default settings
			require "resources/classes/domains.php";
			$domain = new domains();
			$domain->set();
		}

		function __construct0(){
		}

		function __construct1(&$i){
			$this->importer = $i;
		}

		function __destruct(){
			closelog();
		}

		// From PHP NET, Thanks
		private function display_xml_error($error, $xml){
			$return  = $xml[$error->line - 1] . "\n";
			$return .= str_repeat('-', $error->column) . "^\n";

			switch ($error->level) {
				case LIBXML_ERR_WARNING:
					$return .= "Warning $error->code: ";
					break;
				case LIBXML_ERR_ERROR:
					$return .= "Error $error->code: ";
					break;
				case LIBXML_ERR_FATAL:
					$return .= "Fatal Error $error->code: ";
					break;
				}

			$return .= trim($error->message)."\n  Line: $error->line\n  Column: $error->column";

			if ($error->file) {
				$return .= "\n  File: $error->file";
			}
			return "$return\n\n--------------------------------------------\n\n";
		}
	}
}
